package com.sauna;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import com.sauna.exception.IncorrectPathException;
import com.sauna.model.InputPaths;
import com.sauna.util.Utils;
import com.sauna.validator.PathValidator;


public class LetterCollectorIncorectPathTest {
    
    @Test
    @DisplayName("Test more than one start path sign")
    void testIncorrectPath1() {
        assertTrue("Path has more than one start sign!", PathValidator.hasMoreThanOneStartSimbol(Utils.populatePathMap(InputPaths.incorrectPath1)));
    }
    
    @Test
    @DisplayName("Test more than one end path sign")
    void testIncorrectPath2() {
        assertTrue("Path has more than one stop sign!", PathValidator.hasMoreThanOneStopSimbol(Utils.populatePathMap(InputPaths.incorrectPath2)));
    }
    
    @Test
    @DisplayName("Test broken path")
    void testBrokenPath() {
        IncorrectPathException exception = assertThrows(IncorrectPathException.class, () -> SaunaLetterCollector.main(null));
        assertEquals("Broken path!", exception.getMessage());
    }

}
