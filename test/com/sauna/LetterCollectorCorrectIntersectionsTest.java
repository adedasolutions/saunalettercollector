package com.sauna;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedHashMap;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.sauna.exception.IncorrectPathException;
import com.sauna.model.InputPaths;
import com.sauna.service.PathFinder;
import com.sauna.util.Utils;


class LetterCollectorCorrectIntersectionsTest {
    
    private static final String MUST_BE_EQUAL_MSG = "Values must be equal";
    private static final String MUST_NOT_BE_NULL_MSG = "The value must not be null";

    @Test
    @DisplayName("Test traversing 2d correct input path with intersections and collecting allowed signs along the path")
    void testCorrectPath2() throws IncorrectPathException {
        
        String correctPath2 = "@|A+---B--+|+--C-+|||+---D--+|x";
        String[][] raw2DPath2 = InputPaths.correctPath2;
        List<LinkedHashMap<Integer, String>>  populatedPathMap = Utils.populatePathMap(raw2DPath2);        
        String path2ToTest = PathFinder.pathFinder(populatedPathMap);
        
        assertNotNull(populatedPathMap, MUST_NOT_BE_NULL_MSG);
        assertEquals(correctPath2, path2ToTest, MUST_BE_EQUAL_MSG);
    }

}
