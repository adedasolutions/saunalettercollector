package com.sauna.service;

import java.util.LinkedHashMap;
import java.util.List;
import com.sauna.exception.IncorrectPathException;
import com.sauna.util.Utils;
import com.sauna.validator.PathValidator;


public class PathFinder {    
    
    private static List<LinkedHashMap<Integer, String>> path = null;
    private static boolean goUp = false;
    private static boolean goDown = false;
    private static boolean goLeft = false;
    private static boolean goRight = false; 
    private static boolean pathEnd = false;
    private static boolean skip = false;
    private static int maxRowSize = 0;
    private static int maxColumnSize = 0;          
    private static int startRow = 0;
    private static int startColumn = 0;
    private static String finalPath = "";  
    
    
    public static  String pathFinder(List<LinkedHashMap<Integer, String>> p) throws IncorrectPathException {
        path = p;
        goRight = true;
        maxRowSize = path.size();
        maxColumnSize = path.stream().map(hm -> hm.entrySet().size()).reduce(Integer::max).get(); 
        LinkedHashMap<Integer, String> currentRow = null;
        
        for(int i = 0; i < path.size(); i++) { 
            currentRow = path.get(i);  
            if(currentRow.containsValue("@")) {
                startRow = i;                 
                startColumn = Utils.getKeyByValue(currentRow, "@");
                finalPath += "@";
            }
        }
        
        while(!pathEnd) {
                skip = false;
                checkDirection(startRow, startColumn);
        }    
        
        return finalPath;
    }     
    
    public static void checkDirection(int row, int col) throws IncorrectPathException {
        
        LinkedHashMap<Integer, String> currentRow = null;
        LinkedHashMap<Integer, String> nextRow = null;
        LinkedHashMap<Integer, String> prevRow = null;
        
        String valueRight = null;
        String valueLeft = null;
        String valueDown = null;
        String valueUp = null;
        
        if(goRight && !skip) {
            try {
                currentRow = path.get(row);
                int lastColumn = Utils.getLast(currentRow);
                
                if(currentRow != null && col < lastColumn) {
                    valueRight = currentRow.get(col+1);
                    if(valueRight != null && valueRight.equals("")) {
                        throw new IncorrectPathException("Broken path!"); 
                    }
                    if(valueRight != null && PathValidator.isHorizontalDirection(valueRight)) {
                        finalPath += valueRight;
                        System.out.println("Step right: " + finalPath);
                        startRow = row;
                        startColumn = col+1;
                        goRight = true;
                        goUp = false;
                        goDown = false;
                        goLeft = false;
                        skip = true;
                        if(finalPath.contains(" ")) {
                            throw new IncorrectPathException("Broken path!"); 
                        }
                    } if(valueRight != null && PathValidator.isVerticalDirection(valueRight)) {
                        System.out.println("Step right through path intersection");
                        startRow = row;
                        startColumn = col+1;
                        goRight = true;
                        goUp = false;
                        goDown = false;
                        goLeft = false;
                        skip = true;
                    } else if(valueRight != null && PathValidator.isLetter(valueRight)) {
                        finalPath += valueRight;
                        System.out.println("Step right: " + finalPath);
                        startRow = row;
                        startColumn = col+1;
                        goRight = true;
                        goUp = true;
                        goDown = true;
                        goLeft = false;
                        skip = true;                        
                    } else if(valueRight != null && PathValidator.isTurnSimbol(valueRight)) {
                        finalPath += valueRight;
                        System.out.println("Step right: " + finalPath);
                        startRow = row;
                        startColumn = col+1;
                        goRight = false;
                        goUp = true;
                        goDown = true;
                        goLeft = false;
                        skip = true;
                    }  else if(valueRight != null && PathValidator.isEndSimbol(valueRight) ) {
                        finalPath += valueRight;
                        System.out.println("Step right: " + finalPath);
                        pathEnd = true;
                        skip = true;
                    } 
                } else if (currentRow != null && currentRow.size()-1 == col ) {
                    goRight = false;
                    goUp = true;
                    goDown = true;
                    goLeft = false;
                    skip = true;
                } 
            } catch ( IndexOutOfBoundsException e ) {
            } 
        } 

        if(goUp && !skip) {
            try {
                prevRow = path.get(row-1);
                if(prevRow != null && 0 < row && row <= maxRowSize) {
                    valueUp = prevRow.get(col);
                    if(valueUp != null && valueUp.equals("")) {
                        throw new IncorrectPathException("Broken path!"); 
                    }
                    if(valueUp != null && PathValidator.isVerticalDirection(valueUp)) {
                        finalPath += valueUp;
                        System.out.println("Step up:    " + finalPath);
                        startRow = row-1;
                        startColumn = col;
                        goRight = false;
                        goUp = true;
                        goDown = false;
                        goLeft = false;
                        skip = true;
                    } if(valueUp != null && PathValidator.isHorizontalDirection(valueUp)) {
                        System.out.println("Step up through path intersection");
                        startRow = row-1;
                        startColumn = col;
                        goRight = false;
                        goUp = true;
                        goDown = false;
                        goLeft = false;
                        skip = true;
                    } else if(valueUp != null && PathValidator.isLetter(valueUp)) {
                        finalPath += valueUp;
                        System.out.println("Step up:    " + finalPath);
                        startRow = row-1;
                        startColumn = col;
                        goRight = true;
                        goUp = false;
                        goDown = true;
                        goLeft = true;
                        skip = true;
                    } else if(valueUp != null && PathValidator.isTurnSimbol(valueUp)) {
                        finalPath += valueUp;
                        System.out.println("Step up:    " + finalPath);
                        startRow = row-1;
                        startColumn = col;
                        goRight = true;
                        goUp = false;
                        goDown = false;
                        goLeft = true;
                        skip = true;
                    } else if(valueUp != null && PathValidator.isEndSimbol(valueUp) ) {
                        finalPath += valueUp;
                        System.out.println("Step up:    " + finalPath);
                        pathEnd = true;
                        skip = true;
                    }
                }
            } catch ( IndexOutOfBoundsException e ) {
            } 
        } 

        if(goDown && !skip) {
            try {
                nextRow = path.get(row+1);
                if(nextRow != null && row < maxRowSize) {
                    valueDown = nextRow.get(col);
                    if(valueDown != null && valueDown.equals("")) {
                        throw new IncorrectPathException("Broken path!"); 
                    }
                    if(valueDown != null && PathValidator.isVerticalDirection(valueDown)) {
                        finalPath += valueDown;
                        System.out.println("Step down:  " + finalPath);
                        startRow = row+1;
                        startColumn = col;
                        goRight = false;
                        goUp = false;
                        goDown = true;
                        goLeft = false;
                        skip = true;
                    } if(valueDown != null && PathValidator.isHorizontalDirection(valueDown)) {
                        System.out.println("Step down through path intersection");
                        startRow = row+1;
                        startColumn = col;
                        goRight = false;
                        goUp = false;
                        goDown = true;
                        goLeft = false;
                        skip = true;
                    } else if(valueDown != null && PathValidator.isLetter(valueDown)) {
                        finalPath += valueDown;
                        System.out.println("Step down:  " + finalPath);
                        startRow = row+1;
                        startColumn = col;
                        goRight = true;
                        goUp = false;
                        goDown = true;
                        goLeft = true;
                        skip = true;
                    } else if(valueDown != null && PathValidator.isTurnSimbol(valueDown)) {
                        finalPath += valueDown;
                        System.out.println("Step down:  " + finalPath);
                        startRow = row+1;
                        startColumn = col;
                        goRight = true;
                        goUp = false;
                        goDown = false;
                        goLeft = true;
                        skip = true;
                    } else if(valueDown != null && PathValidator.isEndSimbol(valueDown) ) {
                        finalPath += valueDown;
                        System.out.println("Step down:  " + finalPath);
                        pathEnd = true;
                        skip = true;
                    }
                }
            } catch ( IndexOutOfBoundsException e ) {
            } 
        }
        
        if(goLeft && !skip) {
            try {
                currentRow = path.get(row);
                if(currentRow != null && 0 < col && col <= maxColumnSize) {
                    valueLeft = currentRow.get(col-1);                    
                    //int prevKey = Utils.getKeyByValue(currentRow, valueLeft);
                    if(valueLeft != null && valueLeft.equals("")) {
                        throw new IncorrectPathException("Broken path!"); 
                    }
                    if(valueLeft != null && PathValidator.isHorizontalDirection(valueLeft)) {
                        finalPath += valueLeft;
                        System.out.println("Step left:  " + finalPath);
                        startRow = row;
                        startColumn = col-1;
                        goRight = false;
                        goUp = false;
                        goDown = false;
                        goLeft = true;
                        skip = true;
                    } if(valueLeft != null && PathValidator.isVerticalDirection(valueLeft)) {
                        System.out.println("Step left through path intersection");
                        startRow = row;
                        startColumn = col-1;
                        goRight = false;
                        goUp = false;
                        goDown = false;
                        goLeft = true;
                        skip = true;
                    } else if(valueLeft != null && PathValidator.isLetter(valueLeft)) {
                        finalPath += valueLeft;
                        System.out.println("Step left:  " + finalPath);
                        startRow = row;
                        startColumn = col-1;
                        goRight = false;
                        goUp = true;
                        goDown = true;
                        goLeft = true;
                        skip = true;
                    } else if(valueLeft != null && PathValidator.isTurnSimbol(valueLeft)) {
                        finalPath += valueLeft;
                        System.out.println("Step left:  " + finalPath);
                        startRow = row;
                        startColumn = col-1;
                        goRight = false;
                        goUp = true;
                        goDown = true;
                        goLeft = false;
                        skip = true;
                    } else if(valueLeft != null && PathValidator.isEndSimbol(valueLeft) ) {
                        finalPath += valueLeft;
                        System.out.println("Step left:  " + finalPath);
                        pathEnd = true;
                        skip = true;
                    }
                }
            } catch ( IndexOutOfBoundsException e ) {
            } 
        } 
    }    
}
