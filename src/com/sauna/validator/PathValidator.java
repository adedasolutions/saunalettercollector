package com.sauna.validator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class PathValidator { 
    
    public static boolean hasMoreThanOneStartSimbol(List<LinkedHashMap<Integer, String>> path) {
        boolean hasMoreThanOneStartSimbol = false;
        int startPathChars = path.stream().flatMap(m -> m.entrySet().stream().map(v -> v.getValue()).filter(f -> f.equals("@"))).collect(Collectors.toList()).size();
        
        if(startPathChars > 1) {
            hasMoreThanOneStartSimbol = true;
        }
        return hasMoreThanOneStartSimbol;
    }
    
    public static boolean hasMoreThanOneStopSimbol(List<LinkedHashMap<Integer, String>> path) {
        boolean hasMoreThanOneStopSimbol = false;
        int startPathChars = path.stream().flatMap(m -> m.entrySet().stream().map(v -> v.getValue()).filter(f -> f.equals("x"))).collect(Collectors.toList()).size();
        
        if(startPathChars > 1) {
            hasMoreThanOneStopSimbol = true;
        }
        return hasMoreThanOneStopSimbol;
    }    
    
    public static boolean isStartSimbol(String input) {
        boolean inputMatches = true;
        Pattern inputRegex = Pattern.compile("[@]");
        
        if (!inputRegex.matcher(input).matches()) {
            inputMatches = false;
        }
        return inputMatches;
    }
    
    public static boolean isEndSimbol(String input) {
        boolean inputMatches = true;        
        Pattern inputRegex = Pattern.compile("[x]");
        
        if (!inputRegex.matcher(input).matches()) {
            inputMatches = false;
        }
        return inputMatches;
    }
    
    public static boolean isLetter(String input) {
        boolean inputMatches = true;
        Pattern inputRegex = Pattern.compile("[A-Z]");
        
        if (!inputRegex.matcher(input).matches()) {
            inputMatches = false;
        }
        return inputMatches;
    }
    
    public static boolean isHorizontalDirection(String input) {
        boolean inputMatches = true;
        Pattern inputRegex = Pattern.compile("[\\-]");
        
        if (!inputRegex.matcher(input).matches()) {
            inputMatches = false;
        }
        return inputMatches;
    }
    
    public static boolean isVerticalDirection(String input) {
        boolean inputMatches = true;
        Pattern inputRegex = Pattern.compile("[\\\\|]");
        
        if (!inputRegex.matcher(input).matches()) {
            inputMatches = false;
        }
        return inputMatches;
    }
    
    public static boolean isTurnSimbol(String input) {
        boolean inputMatches = true;
        Pattern inputRegex = Pattern.compile(".*[+].*");
        
        if (!inputRegex.matcher(input).matches()) {
            inputMatches = false;
        }
        return inputMatches;
    }
}
