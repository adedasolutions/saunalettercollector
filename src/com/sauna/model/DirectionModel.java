package com.sauna.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DirectionModel {
    
    private boolean isRow;
    private LinkedHashMap<Integer, String> rowToOperate;
    private Map<Integer, List<String>> columnToOperate;
    private String direction;
    private int currentRow;
    private int currentColumn;

    public DirectionModel() {
    }

    public DirectionModel(boolean isRow, LinkedHashMap<Integer, String> rowToOperate, Map<Integer, List<String>> columnToOperate, String direction, int currentRow, int currentColumn) {
        this.isRow = isRow;
        this.columnToOperate = columnToOperate;
        this.rowToOperate = rowToOperate;
        this.direction = direction;
        this.currentRow = currentRow;
        this.currentColumn = currentColumn;
    }    
    
    
    public boolean isRow() {
        return isRow;
    }

    public void setRow(boolean isRow) {
        this.isRow = isRow;
    }

    public LinkedHashMap<Integer, String> getRowToOperate() {
        return rowToOperate;
    }

    public void setRowToOperate(LinkedHashMap<Integer, String> rowToOperate) {
        this.rowToOperate = rowToOperate;
    }

    public Map<Integer, List<String>> getColumnToOperate() {
        return columnToOperate;
    }

    public void setColumnToOperate(Map<Integer, List<String>> columnToOperate) {
        this.columnToOperate = columnToOperate;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public int getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(int currentRow) {
        this.currentRow = currentRow;
    }

    public int getCurrentColumn() {
        return currentColumn;
    }

    public void setCurrentColumn(int currentColumn) {
        this.currentColumn = currentColumn;
    }
}
