package com.sauna;

import java.util.List;
import java.util.stream.Collectors;
import com.sauna.exception.IncorrectPathException;
import com.sauna.model.InputPaths;
import com.sauna.service.PathFinder;
import com.sauna.util.Utils;
import com.sauna.validator.PathValidator;


public class SaunaLetterCollector {

    public static void main(String[] args) throws IncorrectPathException { 
        
        System.out.println("Input path: "); 
        Utils.printPathMap(InputPaths.correctPath2);
        
        if(!PathValidator.hasMoreThanOneStartSimbol(Utils.populatePathMap(InputPaths.correctPath2)) &&
               !PathValidator.hasMoreThanOneStopSimbol(Utils.populatePathMap(InputPaths.correctPath2))) 
        {
            String path = PathFinder.pathFinder(Utils.populatePathMap(InputPaths.correctPath2));
            List<String> onlyLetters = path.codePoints().mapToObj(c -> String.valueOf((char) c)).filter(PathValidator::isLetter).collect(Collectors.toList());

            System.out.println("Final path: " + path);             
            System.out.println("Only letters: " + onlyLetters);
            
        } else {
           throw new IncorrectPathException("Incorrect path!"); 
        }

    }
}