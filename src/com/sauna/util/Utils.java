package com.sauna.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Map.Entry;


public class Utils {
    
    public static void printPathMap(String[][] path) {
        for (int row = 0; row < path.length; row++) { 
            for (int col = 0; col < path[row].length; col++) {
                System.out.print(path[row][col]);                
            } 
            System.out.println(); 
        }    
    }
    
    public static List<LinkedHashMap<Integer, String>>  populatePathMap(String[][] path) {
        List<LinkedHashMap<Integer, String>> list = new ArrayList<LinkedHashMap<Integer, String>>();
        for (int row = 0; row < path.length; row++) { 
            LinkedHashMap<Integer, String> rowReduced = new LinkedHashMap<Integer, String>();
            for (int col = 0; col < path[row].length; col++) {
                if(path[row][col] != " ") {
                    rowReduced.put(col, path[row][col]);
                } else if(path[row][col] == "") {
                    rowReduced.put(col, path[row][col]);
                }  
            } 
            list.add(rowReduced);
        }
        return list;    
    }
    
    public static <T, E> T getKeyByValue(LinkedHashMap<T, E> map, E value) {
        for (Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
    
    public static <T, E> T  getLast(LinkedHashMap<T, E> lhm) {
        int count = 1;
        for (Entry<T, E> it : lhm.entrySet()) {
            if (count == lhm.size()) {
                return it.getKey();
            }
            count++;
        }
        return null;
    }
}
