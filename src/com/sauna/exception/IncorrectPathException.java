package com.sauna.exception;


public class IncorrectPathException extends Exception {
    
    private static final long serialVersionUID = 1L;

    public IncorrectPathException(String errorMessage) {
        super(errorMessage);
    }
}
